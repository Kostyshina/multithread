package by.epam.multithread.tunnel;

import by.epam.multithread.exception.TechnicException;
import by.epam.multithread.thread.Train;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Semaphore;

/**
 * Two blocking queues and work with it
 */
public class TrainQueue {
    private Semaphore mutexLeftQueue;
    private Semaphore mutexRightQueue;
    private Queue<Train> leftTrains;
    private Queue<Train> rightTrains;
    {
        mutexLeftQueue = new Semaphore(1);
        mutexRightQueue = new Semaphore(1);
        leftTrains = new ArrayDeque<>();
        rightTrains = new ArrayDeque<>();
    }

    public void addRightTrain(Train train) throws TechnicException{
        try{
            mutexRightQueue.acquire();
            rightTrains.add(train);
        } catch (InterruptedException e){
            throw new TechnicException(e);
        } finally {
            mutexRightQueue.release();
        }
    }

    public void addLeftTrain(Train train) throws TechnicException{
        try{
            mutexLeftQueue.acquire();
            leftTrains.add(train);
        } catch (InterruptedException e){
            throw new TechnicException(e);
        } finally {
            mutexLeftQueue.release();
        }
    }

    public void removeRightTrain()throws TechnicException{
        if(!rightTrains.isEmpty()) {
            try {
                mutexRightQueue.acquire();
                rightTrains.remove();
            } catch (InterruptedException e){
                throw new TechnicException(e);
            } finally {
                mutexRightQueue.release();
            }
        }
    }

    public void removeRightTrain(Train train)throws TechnicException{
        if(!rightTrains.isEmpty()) {
            try {
                mutexRightQueue.acquire();
                rightTrains.remove(train);
            } catch (InterruptedException e){
                throw new TechnicException(e);
            } finally {
                mutexRightQueue.release();
            }
        }
    }
    public void removeLeftTrain() throws TechnicException{
        if(!leftTrains.isEmpty()) {
            try {
                mutexLeftQueue.acquire();
                leftTrains.remove();
            } catch (InterruptedException e){
                throw new TechnicException(e);
            } finally {
                mutexLeftQueue.release();
            }
        }
    }

    public void removeLeftTrain(Train train) throws TechnicException{
        if(!leftTrains.isEmpty()) {
            try {
                mutexLeftQueue.acquire();
                leftTrains.remove(train);
            } catch (InterruptedException e){
                throw new TechnicException(e);
            } finally {
                mutexLeftQueue.release();
            }
        }
    }

    public boolean isPeekRight(Train train) throws TechnicException{
        try{
            mutexRightQueue.acquire();
            return !rightTrains.isEmpty() && rightTrains.element().equals(train);
        } catch (InterruptedException e){
            throw new TechnicException(e);
        } finally {
            mutexRightQueue.release();
        }
    }

    public boolean isPeekLeft(Train train) throws TechnicException{
        try{
            mutexLeftQueue.acquire();
            return !leftTrains.isEmpty() && leftTrains.element().equals(train);
        } catch (InterruptedException e){
            throw new TechnicException(e);
        } finally {
            mutexLeftQueue.release();
        }
    }
}
