package by.epam.multithread.tunnel;

import by.epam.multithread.exception.TechnicException;
import by.epam.multithread.thread.Train;
import org.apache.log4j.Logger;

/**
 * Two blocking queues for each Tunnel's object
 * @see by.epam.multithread.tunnel.TrainQueue
 */
public class Tunnel {
    public static final Logger LOG = Logger.getLogger(Tunnel.class);
    private TrainQueue trainQueue;
    {
        trainQueue = new TrainQueue();
    }

    //можно сделать метод с передачей мьютекса и очереди
    /**
     * Add train to the right queue
     * @param direction Which queue (right or left)
     * @param train Current thread
     */
    public Tunnel addTrain(String direction, Train train){
        try {
            if ("left".equalsIgnoreCase(direction)) {
                trainQueue.addLeftTrain(train);
            } else if ("right".equalsIgnoreCase(direction)) {
                trainQueue.addRightTrain(train);
            }
        } catch (TechnicException e){
            LOG.error(e);
            //System.out.println("Interrupted exception");
        }
        return this;
    }

    /**
     * Remove first train from the queue
     * @param direction Which queue (right or left)
     * @return true if the removal was successful
     */
    public boolean removeTrain(String direction){
        try {
            if ("left".equalsIgnoreCase(direction)) {
                trainQueue.removeLeftTrain();
            } else if ("right".equalsIgnoreCase(direction)) {
                trainQueue.removeRightTrain();
            }
        } catch (TechnicException e){
            LOG.error(e);
            //System.out.println("Interrupted exception");
        }
        return false;
    }

    /**
     * Checking whether an item in the queue
     * @param direction Which queue (right or left)
     * @param train Current thread
     * @return boolean value to note if element in the beginning or not
     */
    public boolean isPeekTrain(String direction, Train train){
        try {
            if ("left".equalsIgnoreCase(direction)) {
                return trainQueue.isPeekLeft(train);
            } else if ("right".equalsIgnoreCase(direction)) {
                return trainQueue.isPeekRight(train);
            }
        }catch (TechnicException e){
            LOG.error(e);
            //System.out.println("Interrupted exception");
        }
        return false;
    }

    /**
     * Change the queue, change the tunnel
     * @param direction Which queue (right or left)
     * @param train Current thread
     */
    public void toAnotherTunnel(String direction, Train train){
        try {
            if ("left".equalsIgnoreCase(direction)) {
                trainQueue.removeLeftTrain(train);
            } else if ("right".equalsIgnoreCase(direction)) {
                trainQueue.removeRightTrain(train);
            }
        }catch (TechnicException e){
            LOG.error(e);
            //System.out.println("Interrupted exception");
        }
    }
}
