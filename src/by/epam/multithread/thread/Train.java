package by.epam.multithread.thread;

import by.epam.multithread.exception.TechnicException;
import by.epam.multithread.tunnel.Tunnel;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Each train with name and direction - some thread
 */
public class Train extends Thread {
    public static final Logger LOG = Logger.getLogger(Train.class);

    //private static ArrayList<Tunnel> tunnels;
    //private static ArrayList<Lock> lockingTunnel;

    private static Tunnel tunnelOne = new Tunnel();
    private static Tunnel tunnelTwo = new Tunnel();
    private static Lock lockingTunnelOne;
    private static Lock lockingTunnelTwo;
    static {
        //tunnels = new ArrayList<Tunnel>(2);
        //lockingTunnel = new ArrayList<Lock>(2);
        lockingTunnelOne = new ReentrantLock();
        lockingTunnelTwo = new ReentrantLock();
    }
    private String direction;

    public Train(String name, String direction) {
        super(name);
        this.direction = direction;
    }

    /**
     * Method to run through the each tunnel
     * @param tunnel Some tunnel
     * @param locking lock for tunnel
     * @return boolean value to note the successful passage
     * @throws TechnicException
     */
    private boolean runTunnel(Tunnel tunnel, Lock locking) throws TechnicException{
        try {
            if (locking.tryLock(10000, TimeUnit.MILLISECONDS)) {
                if(tunnel.isPeekTrain(direction, this)) {
                    tunnel.removeTrain(direction);
                    System.out.println(this.getName() + " run threw the tunnel two");
                    LOG.info(this.getName() + " run threw the tunnel two");
                    return true;
                }
            }
            else {
                tunnel.toAnotherTunnel(direction, this);
            }
        } catch(InterruptedException e){
            throw new TechnicException(e);
        } finally {
            locking.unlock();
        }
        return false;
    }

    @Override
    public void run() {
        do {
            try {
                tunnelOne.addTrain(direction, this);
                System.out.println(this.getName() + " added to the queue one");
                LOG.info(this.getName() + " added to the queue one");
                if (runTunnel(tunnelOne, lockingTunnelOne)) {
                    break;
                } else {
                    tunnelTwo.addTrain(direction, this);
                    System.out.println(this.getName() + " added to the queue two");
                    LOG.info(this.getName() + " added to the queue two");
                    if (runTunnel(tunnelTwo, lockingTunnelTwo)) {
                        break;
                    }
                }
            }catch (TechnicException e){
                LOG.error(e);
            }
        }while(true);
    }
}
