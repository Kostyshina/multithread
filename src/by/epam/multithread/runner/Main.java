package by.epam.multithread.runner;

import by.epam.multithread.thread.Train;
import org.apache.log4j.PropertyConfigurator;

/**
 * @author Lisa Kostyshina
 * @version 1.2
 */
public class Main {
    static{
        PropertyConfigurator.configure("log4j.properties");
    }

    public static void main(String[] args) {
        Train [] leftTrains = new Train[10];
        Train [] rightTrains = new Train[10];
        for(int i=0, j=0 ; i < 10; i++, j++){
            leftTrains[i] = new Train("trainLeft"+i,"left");
            rightTrains[j] = new Train("trainRight"+j, "right");
            leftTrains[i].start();
            rightTrains[j].start();
        }
    }
}
