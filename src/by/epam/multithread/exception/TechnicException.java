package by.epam.multithread.exception;

/**
 * Technical exception
 */
public class TechnicException extends Exception {
    public TechnicException() {
    }

    public TechnicException(String message) {
        super(message);
    }

    public TechnicException(String message, Throwable cause) {
        super(message, cause);
    }

    public TechnicException(Throwable cause) {
        super(cause);
    }

    public TechnicException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
